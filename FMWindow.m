#import <UIKit/UIKit.h>
#import "FMWindow.h"

@implementation FMWindow

- (instancetype)init {
  self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
  if (self != nil){
  	[self setHidden:NO];
    [self setWindowLevel:UIWindowLevelAlert];
    [self setRootViewController:[FMViewController sharedInstance]];
  	[self setBackgroundColor:[UIColor clearColor]];
  	[self setUserInteractionEnabled:YES];
  }
  return self;
}

-(void)makeKeyAndVisible {
    [super makeKeyAndVisible];
    return;
}
-(bool)_shouldCreateContextAsSecure {
    return 0x0;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
  UIView *hitTestResult = [super hitTest:point withEvent:event];
  if([hitTestResult isEqual:[[FMViewController sharedInstance] view]] || [hitTestResult isEqual:[FMMusicController sharedInstance]]) return nil;
  return hitTestResult;
}

+ (instancetype)sharedInstance {
  static dispatch_once_t p = 0;
  __strong static id _sharedSelf = nil;
  dispatch_once(&p, ^{
    _sharedSelf = [[self alloc] init];
  });
  return _sharedSelf;
}

@end
